Traduccion Readme.txt

#PrjVigiaPrecios



## Empezando

Para que le resulte más fácil comenzar con GitLab, aquí hay una lista de los próximos pasos recomendados.

¿Ya eres un profesional? Simplemente edite este archivo README.md y personalícelo. ¿Quieres hacerlo fácil? [Use la plantilla en la parte inferior](#editando-este-readme)!

## Agrega tus archivos

- [ ] [Crear](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) o [subir](https://docs.gitlab.com/ ee/user/project/repository/web_editor.html#upload-a-file) archivos
- [ ] [Agregar archivos usando la línea de comandos](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) o empujar un repositorio Git existente con el siguiente comando:

```
cd existing_repo
git remote add origin https://gitlab.com/minticr2c4/prjvigiaprecios.git
git branch -M main
git push -uf origin main
```

## Integre con sus herramientas

- [ ] [Configurar integraciones de proyectos](https://gitlab.com/minticr2c4/prjvigiaprecios/-/settings/integrations)

## Colabora con tu equipo

- [ ] [Invitar a miembros del equipo y colaboradores](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Crear una nueva solicitud de combinación](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Cerrar automáticamente los problemas de las solicitudes de combinación](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Habilitar aprobaciones de solicitud de fusión](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Combinar automáticamente cuando la canalización tiene éxito](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Probar e implementar

Utilice la integración continua incorporada en GitLab.

- [ ] [Empezar con GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analice su código en busca de vulnerabilidades conocidas con Pruebas de seguridad de aplicaciones estáticas (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Implementar en Kubernetes, Amazon EC2 o Amazon ECS mediante Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Utilice implementaciones basadas en extracción para mejorar la gestión de Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Configurar entornos protegidos](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editando este README

Cuando esté listo para hacer suyo este LÉAME, simplemente edite este archivo y use la práctica plantilla a continuación (o siéntase libre de estructurarlo como desee, ¡esto es solo un punto de partida!). Gracias a makeareadme.com por esta plantilla.

## Sugerencias para un buen README

Cada proyecto es diferente, así que considere cuál de estas secciones se aplica al suyo. Las secciones utilizadas en la plantilla son sugerencias para la mayoría de los proyectos de código abierto. También tenga en cuenta que si bien un LÉAME puede ser demasiado largo y detallado, es mejor demasiado largo que demasiado corto. Si cree que su LÉAME es demasiado largo, considere utilizar otra forma de documentación en lugar de recortar información.


## Nombre
Elija un nombre que se explique por sí mismo para su proyecto.

## Descripción
Deja que la gente sepa lo que tu proyecto puede hacer específicamente. Proporcione contexto y agregue un enlace a cualquier referencia con la que los visitantes no estén familiarizados. También se puede agregar aquí una lista de Características o una subsección de Antecedentes. Si existen alternativas a su proyecto, este es un buen lugar para enumerar los factores diferenciadores.

## Insignias
En algunos archivos README, es posible que vea imágenes pequeñas que transmiten metadatos, como si todas las pruebas están pasando o no para el proyecto. Puede usar Shields para agregar algunos a su README. Muchos servicios también tienen instrucciones para agregar una insignia.

## Visuales
Dependiendo de lo que esté haciendo, puede ser una buena idea incluir capturas de pantalla o incluso un video (con frecuencia verá GIF en lugar de videos reales). Herramientas como ttygif pueden ayudar, pero consulte Asciinema para obtener un método más sofisticado.

## Instalación
Dentro de un ecosistema particular, puede haber una forma común de instalar cosas, como usar Yarn, NuGet o Homebrew. Sin embargo, considere la posibilidad de que quien esté leyendo su LÉAME sea un novato y desee más orientación. Enumerar pasos específicos ayuda a eliminar la ambigüedad y hace que las personas usen su proyecto lo más rápido posible. Si solo se ejecuta en un contexto específico, como una versión de lenguaje de programación o un sistema operativo en particular, o tiene dependencias que deben instalarse manualmente, agregue también una subsección de Requisitos.

## Uso
Use ejemplos generosamente y muestre el resultado esperado si puede. Es útil tener en línea el ejemplo más pequeño de uso que pueda demostrar, al tiempo que proporciona enlaces a ejemplos más sofisticados si son demasiado largos para incluirlos razonablemente en el LÉAME.

## Apoyo
Dígales a las personas a dónde pueden acudir para obtener ayuda. Puede ser cualquier combinación de un rastreador de problemas, una sala de chat, una dirección de correo electrónico, etc.

## Mapa vial
Si tiene ideas para lanzamientos en el futuro, es una buena idea enumerarlas en el LÉAME.

## contribuyendo
Indique si está abierto a las contribuciones y cuáles son sus requisitos para aceptarlas.
Para las personas que desean realizar cambios en su proyecto, es útil tener alguna documentación sobre cómo comenzar. Tal vez haya un script que deban ejecutar o algunas variables de entorno que deban configurar. Haga estos pasos explícitos. Estas instrucciones también podrían ser útiles para tu futuro yo.
También puede documentar comandos para borrar el código o ejecutar pruebas. Estos pasos ayudan a garantizar una alta calidad del código y reducen la probabilidad de que los cambios rompan algo sin darse cuenta. Tener instrucciones para ejecutar pruebas es especialmente útil si requiere una configuración externa, como iniciar un servidor Selenium para realizar pruebas en un navegador.

## Autores y reconocimiento
Muestre su agradecimiento a aquellos que han contribuido al proyecto.

## Licencia
Para proyectos de código abierto, diga cómo se licencia.

## Estado del proyecto
Si se quedó sin energía o sin tiempo para su proyecto, coloque una nota en la parte superior del LÉAME que diga que el desarrollo se ha ralentizado o se ha detenido por completo. Alguien puede optar por bifurcar su proyecto o ser voluntario para intervenir como mantenedor o propietario, lo que permite que su proyecto continúe. También puede hacer una solicitud explícita de main
