import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:prjvigiaprecios/views/pages/search_product/search_product_page.dart';
import 'package:prjvigiaprecios/views/prjbacklayout.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(const MaterialApp(
      title: 'Vigía Precios',
      debugShowCheckedModeBanner: false,
      home: PrjBackLayout(
        caption: 'Vigía de Precios',
        content: SearchProductPage(),
      )));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Vigia Precios',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const SearchProductPage(),
    );
  }
}
