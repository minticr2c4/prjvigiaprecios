class CommonValidator {
  static String? required(String? value) {
    if (value == null || value.isEmpty) {
      return "El campo es obligatorio";
    }
    return null;
  }

  static String? nullable(String? val) {
    return null;
  }

  static String? mustBeCoords(String? val) {
    var rgx = RegExp(r"^-?\d+\.\d+$");
    if (!rgx.hasMatch(val ?? "")) {
      return "Este campo debe contener una coordenada válida";
    }
    return null;
  }

  static String? mustBeCoordsNullable(String? val) {
    if (val == null || val.isEmpty) return null;
    return mustBeCoords(val);
  }

  static String? mustBeEmail(String? val) {
    var rgx = RegExp(r"^[^@]+@[^@]+\.[a-zA-Z]{2,}$");
    if (!rgx.hasMatch(val ?? "")) {
      return "Este campo debe contener correo electrónico válido";
    }
    return null;
  }

  static String? mustBeEmailNullable(String? val) {
    if (val == null || val.isEmpty) return null;
    return mustBeEmail(val);
  }

  static String? mustBeUrl(String? val) {
    var rgx = RegExp(r"^https?:\/\/[\w\-]+(\.[\w\-]+)+[/#?]?.*$");
    if (!rgx.hasMatch(val ?? "")) {
      return "Este campo debe contener una Url válida";
    }
    return null;
  }

  static String? mustBeUrlNullable(String? val) {
    if (val == null || val.isEmpty) return null;
    return mustBeUrl(val);
  }
}
