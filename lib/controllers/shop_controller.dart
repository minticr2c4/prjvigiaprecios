import '../models/entities/shop.dart';
import '../models/repositories/shop_repository.dart';

class ShopController {
  late ShopRepository _repo;

  ShopController() {
    _repo = ShopRepository();
  }

  Future<void> save(Shop shop) async {
    await _repo.newShop(shop);
  }

  Future<List<Shop>> listAll() async {
    return await _repo.getAll();
  }
}
