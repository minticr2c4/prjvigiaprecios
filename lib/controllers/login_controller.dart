// import '../model/entity/user.dart';
// import '../model/repository/firebaseauth.dart';
// import '../model/repository/usuarios.dart';
// import 'request/login_request.dart';
// import 'request/signup_request.dart';
// import 'response/user_info_response.dart';

import 'package:prjvigiaprecios/controllers/response/user_response.dart';
import 'package:prjvigiaprecios/models/repositories/login_authentication.dart';
import 'package:prjvigiaprecios/models/repositories/user_repository.dart';

import '../models/entities/user_entity.dart';

class LoginController {
  //late Usuario _reporitory;
  late LoginAuthentication _authRepository;
  late UserRepository _user;

  LoginController() {
    _authRepository = LoginAuthentication();
    _user = UserRepository();
  }

  //   _reporitory = UsuarioReporitory();
  //   _authRepository = FirebaseAuthRepository();
  String? isRequired(String? value) {
    if (value == null || value.isEmpty) {
      return "El campo es obligatorio";
    }
    return null;
  }

  String? nullable(String? val) {
    return null;
  }

  Future<int> validateUser(String? email, String? pass) async {
    return await LoginAuthentication()
        .signInEmailPassword(email ?? "", pass ?? "");
  }
}

  // Future<String> validarC