class LoginRequest {
  late String? email;
  late String? password;

  @override
  String toString() => "$email / $password";
}
