import 'package:prjvigiaprecios/models/entities/product.dart';
import 'package:prjvigiaprecios/models/repositories/product_repository.dart';

class ProductController {
  late ProductRepository _repo;

  ProductController() {
    _repo = ProductRepository();
  }

  Future<void> save(Product product) async {
    await _repo.newProduct(product);
  }

  Future<List<Product>> listAll() async {
    return await _repo.getAll();
  }

  Future<List<Product>?> getProducts() async {
    List<Product>? productsFound = await ProductRepository().getAll();

    if (productsFound.isEmpty) {
      return null;
    }

    return productsFound;
  }
}
