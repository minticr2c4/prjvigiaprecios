import '../../models/entities/product.dart';

class ProductRequest {
  late String name;
  late String category;
  late String largeName;
  late String brand;
  late String description;

  Product toProduct() {
    return Product(
        name: name,
        category: category,
        largeName: largeName,
        brand: brand,
        description: description);
  }
}
