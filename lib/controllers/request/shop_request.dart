import '../../models/entities/shop.dart';

class ShopRequest {
  late String name;
  late String? email;
  late num? phone;
  late double latitude;
  late double longitude;
  late String? address;
  late String? image;

  Shop toShop() {
    return Shop(
        name: name,
        latitude: latitude,
        longitude: longitude,
        email: email,
        address: address,
        phone: phone,
        image: image);
  }
}
