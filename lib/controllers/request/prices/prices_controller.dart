import 'package:prjvigiaprecios/models/entities/price_entity.dart';
import 'package:prjvigiaprecios/models/repositories/prices_repository.dart';
import 'package:prjvigiaprecios/models/entities/search_product.dart';
import 'package:prjvigiaprecios/models/repositories/product_repository.dart';
import 'package:prjvigiaprecios/models/entities/product.dart';
import 'package:prjvigiaprecios/models/entities/product_shop.dart';
import 'package:prjvigiaprecios/models/entities/shop.dart';
import 'package:prjvigiaprecios/models/repositories/shop_repository.dart';
import 'package:flutter/material.dart';

class PricesController {
  Future<List<ProductShop>?> getPricesByName(
      SearchProduct searchProduct) async {
    List<Product>? productsFound =
        await ProductRepository().getByName(searchProduct.name);

    if (productsFound!.isEmpty) {
      debugPrint('No hay productos');
      return null;
    }

    List<ProductShop>? productsAndShops = [];

    for (Product product in productsFound) {
      debugPrint('Producto: ${product.largeName} ${product.brand}');

      List<Price>? prices =
          await PricesRepository().getByIdProduct(product.id!);

      debugPrint('Prices: ${prices.toString()}');

      for (Price price in prices!) {
        Shop? shop = await ShopRepository().getById(price.idShop);

        if (shop == null) {
          throw Exception('Shop ${price.idShop} not found');
        }

        debugPrint('Tienda que carga $shop');

        ProductShop productShopResponse = ProductShop(
            shop: shop,
            product: product,
            amount: price.amount,
            date: price.date);

        productsAndShops.add(productShopResponse);
      }
    }

    return productsAndShops;
  }
}
