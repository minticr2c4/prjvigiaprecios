import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:prjvigiaprecios/models/entities/shop.dart';

class ShopRepository {
  late final CollectionReference _shops;

  ShopRepository() {
    _shops = FirebaseFirestore.instance.collection("Shops");
  }

  Future<void> newShop(Shop shop) async {
    await _shops
        .withConverter<Shop>(
            fromFirestore: Shop.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .add(shop);
  }

  Future<List<Shop>> getAll() async {
    var query = await _shops
        // .where("user", isEqualTo: id)
        .withConverter<Shop>(
            fromFirestore: Shop.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();

    var sales = query.docs.cast().map<Shop>((e) {
      var sale = e.data();
      sale.id = e.id;
      return sale;
    });

    return sales.toList();
  }

  Future<Shop?> getById(String id) async {
    var query = await _shops
        .doc(id)
        .withConverter<Shop>(
            fromFirestore: Shop.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();
    Shop? shop = query.data();
    shop?.id = query.id;
    return shop;
  }
}
