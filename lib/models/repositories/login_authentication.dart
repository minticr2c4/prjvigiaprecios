import 'package:firebase_auth/firebase_auth.dart';

class LoginAuthentication {
  Future<void> createEmailPasswordAccount(String email, String password) async {
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return Future.error('La contraseña requiere mayor seguridad.');
      } else if (e.code == 'email-already-in-use') {
        return Future.error('Ya existe una cuenta con ese correo');
      }
    }
  }

  Future<int> signInEmailPassword(String email, String password) async {
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      return 200;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found' || e.code == 'wrong-password') {
        return Future.error('Revisar usuario y contraseña');
      }
    }
    return 500;
  }

  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
  }
}
