import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:prjvigiaprecios/models/entities/product.dart';

class ProductRepository {
  late final CollectionReference _products;

  ProductRepository() {
    _products = FirebaseFirestore.instance.collection("Product");
  }

  Future<void> newProduct(Product product) async {
    await _products
        .withConverter<Product>(
            fromFirestore: Product.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .add(product);
  }

  Future<List<Product>> getAll() async {
    var query = await _products
        .withConverter<Product>(
            fromFirestore: Product.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();

    var response = query.docs.cast().map<Product>((e) {
      var product = e.data();
      product.id = e.id;
      return product;
    });

    return response.toList();
  }

  Future<Product?> getById(String id) async {
    var query = await _products
        .doc("/Product/$id")
        .withConverter<Product>(
            fromFirestore: Product.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();
    Product? product = query.data();
    product?.id = query.id;
    return product;
  }

  Future<List<Product>?> getByName(String name) async {
    var query = await _products
        .where('name', isEqualTo: name)
        .withConverter<Product>(
            fromFirestore: Product.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();

    var response = query.docs.cast().map<Product>((e) {
      var product = e.data();
      product.id = e.id;
      return product;
    });

    return response.toList();
  }
}
