import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:prjvigiaprecios/controllers/response/user_response.dart';
import 'package:prjvigiaprecios/models/entities/user_entity.dart';

class UserRepository {
  late final CollectionReference _repo;
  UserRepository() {
    _repo = FirebaseFirestore.instance.collection("Cuostomers");
  }
  Future<UserResponse> getByEmail(String email) async {
    final query = await _repo
        .where("email", isEqualTo: email)
        .withConverter<User>(
            fromFirestore: User.fromFirestore,
            toFirestore: (value, options) => value.toFirestore())
        .get();

    var users = query.docs.cast();

    if (users.isEmpty) {
      return Future.error("Usuario no existe");
    }

    var user = users.first;

    var response = user.data();
    response.id = user.id;

    return response;
    
    //     var user = await _repo
    //     .where("email", isEqualTo: email)
    //     .withConverter(
    //         fromFirestore: User.fromFirestore,
    //         toFirestore: ((value, _) => value.toFirestore()))
    //     .get();

    // var users = user.docs.cast().map<User>((e) {
    //   var s = e.data();
    //   s.id = e.id;
    //   return s;
    // });

    // return users.first.
  }
}
