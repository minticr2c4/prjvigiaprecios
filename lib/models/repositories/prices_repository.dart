import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:prjvigiaprecios/models/entities/price_entity.dart';

class PricesRepository {
  late final CollectionReference prices;

  PricesRepository() {
    prices = FirebaseFirestore.instance.collection("Prices");
  }

  Future<void> newPrice(Price price) async {
    await prices
        .withConverter<Price>(
            fromFirestore: Price.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .add(price);
  }

  Future<List<Price>?> getAll() async {
    var query = await prices
        .withConverter<Price>(
            fromFirestore: Price.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();

    var response = query.docs.cast().map<Price>((e) {
      var price = e.data();
      price.id = e.id;
      return price;
    });

    return response.toList();
  }

  Future<Price?> getById(String id) async {
    var query = await prices
        .doc("/Prices/$id")
        .withConverter<Price>(
            fromFirestore: Price.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();
    Price? price = query.data();
    price?.id = query.id;
    return price;
  }

  Future<List<Price>?> getByIdProduct(String idProduct) async {
    var query = await prices
        .where("idProduct", isEqualTo: idProduct)
        .withConverter<Price>(
            fromFirestore: Price.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();

    var response = query.docs.cast().map<Price>((e) {
      var price = e.data();
      price.id = e.id;
      return price;
    });

    return response.toList();
  }
}
