import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Pruebas extends StatelessWidget {
  final String documentId;

  const Pruebas(this.documentId, {super.key});

  @override
  Widget build(BuildContext context) {
    CollectionReference prueba =
        FirebaseFirestore.instance.collection('prueba');

    return FutureBuilder<DocumentSnapshot>(
      future: prueba.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return const Text("Algo salió mal");
        }

        if (snapshot.hasData && !snapshot.data!.exists) {
          return const Text("El documento no existe");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          return Text(
              "Prueba: ${data['nombre']} ${data['numero']} - ${data['descripcion']}");
        }

        return const Text("Cargando...");
      },
    );
  }
}
