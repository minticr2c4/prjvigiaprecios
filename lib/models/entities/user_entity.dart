import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  late String fullname;
  late String email;
  late String? phone;
  late String password;

  User({
    required this.fullname,
    required this.password,
    required this.email,
    this.phone,
  });

  @override
  String toString() {
    return "$fullname, $email, $phone, $password";
  }

  factory User.fromFirestore(DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options) {
    var data = snapshot.data();

    var location = data?["location"];

    return User(
        fullname: data?["fullName"],
        password: data?["password"],
        email: data?["email"],
        phone: data?["phone"]);
  }

  Map<String, dynamic> toFirestore() {
    return {
      "fullName": fullname,
      if (email != null) "email": email,
      if (phone != null) "phone": phone,
      if (password != null) "password": password
    };
  }
}
