import 'package:cloud_firestore/cloud_firestore.dart';

class Product {
  String? id;
  String name;
  String category;
  String largeName;
  String brand;
  String description;

  Product({
    required this.name,
    required this.category,
    required this.largeName,
    required this.brand,
    required this.description,
  });

  @override
  String toString() {
    return '$name, $category, $largeName, $brand, $description';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is Product &&
        other.name == name &&
        other.category == category &&
        other.largeName == largeName &&
        other.brand == brand &&
        other.description == description;
  }

  String searchBy() {
    return '${name.toLowerCase()} ${category.toLowerCase()}';
  }

  @override
  int get hashCode =>
      Object.hash(category, name, largeName, brand, description);

  factory Product.fromFirestore(DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options) {
    var data = snapshot.data();
    return Product(
        name: data?["name"],
        category: data?["category"],
        largeName: data?["largeName"],
        brand: data?["brand"],
        description: data?["description"]);
  }

  Map<String, dynamic> toFirestore() {
    return {
      "name": name,
      "category": category,
      "largeName": largeName,
      "brand": brand,
      "description": description,
    };
  }
}
