import 'package:cloud_firestore/cloud_firestore.dart';

class Price {
  String? id;
  num amount;
  Timestamp date;
  String idCustomer;
  String idProduct;
  String idShop;

  Price(
      {required this.amount,
      required this.date,
      required this.idCustomer,
      required this.idProduct,
      required this.idShop});

  @override
  String toString() {
    return '$amount, $date, $idCustomer, $idProduct, $idShop';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is Price &&
        other.amount == amount &&
        other.date == date &&
        other.idCustomer == idCustomer &&
        other.idProduct == idProduct &&
        other.idShop == idShop;
  }

  @override
  int get hashCode => Object.hash(amount, date, idCustomer, idProduct, idShop);

  factory Price.fromFirestore(DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options) {
    var data = snapshot.data();
    return Price(
        amount: data?["amount"],
        date: data?["date"],
        idCustomer: data?["idCustomer"],
        idProduct: data?["idProduct"],
        idShop: data?["idShop"]);
  }

  Map<String, dynamic> toFirestore() {
    return {
      "amount": amount,
      "date": date,
      "idCustomer": idCustomer,
      "idProduct": idProduct,
      "idShop": idShop,
    };
  }
}
