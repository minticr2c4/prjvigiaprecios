class SearchProduct {
  SearchProduct({
    required this.name,
    required this.category,
  });

  String name;
  String category;

  @override
  String toString() {
    return '$name, $category';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is SearchProduct &&
        other.name == name &&
        other.category == category;
  }

  @override
  int get hashCode => Object.hash(category, name);
}
