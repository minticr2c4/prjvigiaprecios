import 'package:prjvigiaprecios/models/entities/shop.dart';
import 'package:prjvigiaprecios/models/entities/product.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ProductShop {
  Shop shop;
  Product product;
  num? amount;
  Timestamp? date;

  ProductShop(
      {required this.shop, required this.product, this.amount, this.date});

  @override
  String toString() {
    return '$shop, $product';
  }
}
