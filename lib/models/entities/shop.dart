// import 'dart:html';
// import 'dart:js_util';

import 'package:cloud_firestore/cloud_firestore.dart';

class Shop {
  String? id;
  String name;
  double latitude;
  double longitude;
  String? address;
  String? email;
  num? phone;
  String? image;

  Shop(
      {required this.name,
      required this.latitude,
      required this.longitude,
      this.address,
      this.email,
      this.phone,
      this.image});

  @override
  String toString() {
    return '$name, $latitude, $longitude, $address, $email, $phone, $image';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is Shop &&
        other.name == name &&
        other.latitude == latitude &&
        other.longitude == longitude &&
        other.address == address &&
        other.email == email &&
        other.phone == phone &&
        other.image == image;
  }

  @override
  int get hashCode =>
      Object.hash(name, email, phone, latitude, longitude, address, image);

  factory Shop.fromFirestore(DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options) {
    var data = snapshot.data();

    var location = data?["location"];

    return Shop(
        name: data?["businessName"],
        latitude: location.latitude!,
        longitude: location.longitude!,
        address: data?["address"],
        email: data?["email"],
        phone: data?["phone"],
        image: data?["imageUrl"]);
  }

  Map<String, dynamic> toFirestore() {
    return {
      "businessName": name,
      "location": GeoPoint(latitude, longitude),
      if (address != null) "address": address,
      if (email != null) "email": email,
      if (phone != null) "phone": phone,
      if (image != null) "imageUrl": image
    };
  }
}

// class GeoPoint {
//   late double latitude;
//   late double longitude;

//   GeoPoint(this.latitude, this.longitude);
// }
