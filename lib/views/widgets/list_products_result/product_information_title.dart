import 'package:flutter/material.dart';

class ProductInformationTitle extends StatefulWidget {
  final int productsCount;
  const ProductInformationTitle(this.productsCount, {Key? key})
      : super(key: key);

  @override
  State<ProductInformationTitle> createState() =>
      _ProductInformationHeaderTextState();
}

class _ProductInformationHeaderTextState
    extends State<ProductInformationTitle> {
  @override
  Widget build(BuildContext context) {
    int productsCount = widget.productsCount;
    return Center(
      child: Text(
        'Encontramos $productsCount precios asociados a los productos de interés, pulsa en cada uno para ver más detalles',
        style: const TextStyle(
            fontSize: 15, color: Colors.blueGrey, fontWeight: FontWeight.bold),
      ),
    );
  }
}
