import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:prjvigiaprecios/models/entities/product_shop.dart';

class ProductInformationDrawing extends StatefulWidget {
  final ProductShop productShop;
  const ProductInformationDrawing(this.productShop, {Key? key})
      : super(key: key);

  @override
  State<ProductInformationDrawing> createState() =>
      _ProductInformationDrawingState();
}

class _ProductInformationDrawingState extends State<ProductInformationDrawing> {
  @override
  Widget build(BuildContext context) {
    final numberFormat = NumberFormat.currency(locale: 'es_US', symbol: "\$");
    return Container(
        margin: const EdgeInsets.only(left: 10.0, top: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                    width: 100.0,
                    margin: const EdgeInsets.only(
                        left: 10.0, top: 20.0, bottom: 10.0),
                    child: Expanded(
                        child: Text(widget.productShop.product.largeName,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            softWrap: false,
                            style: const TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontWeight: FontWeight.bold)))),
                Container(
                  width: 100.0,
                  margin: const EdgeInsets.only(
                      left: 10.0, top: 10.0, bottom: 20.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: Text(widget.productShop.shop.name,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              softWrap: false,
                              style: const TextStyle(
                                  fontSize: 10.0, color: Colors.black))),
                      const Text(' - ',
                          style:
                              TextStyle(fontSize: 10.0, color: Colors.black)),
                      const Text('900m',
                          style: TextStyle(fontSize: 10.0, color: Colors.black))
                    ],
                  ),
                )
              ],
            ),
            Container(
                alignment: Alignment.centerRight,
                child: Text(numberFormat.format(widget.productShop.amount),
                    style:
                        const TextStyle(fontSize: 14.0, color: Colors.green)))
          ],
        ));
  }
}
