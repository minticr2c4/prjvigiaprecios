import 'package:flutter/material.dart';

class DataField extends StatelessWidget {
  final String caption;
  final TextInputType fldtype;
  final FormFieldValidator<String?> validate;
  final FormFieldSetter<String?> save;
  final bool? passMode;

  DataField(
      {super.key,
      required this.caption,
      required this.fldtype,
      required this.validate,
      required this.save,
      this.passMode});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        maxLength: 50,
        initialValue: "",
        keyboardType: fldtype,
        obscureText: passMode?? false,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: caption,
        ),
        validator: validate,
        onSaved: save,
        style: const TextStyle(
          fontSize: 18,
        ),
      ),
    );
  }
}
