import 'package:flutter/material.dart';

class BrandSimpleLogo extends StatelessWidget {
  final String? image;
  const BrandSimpleLogo(this.image, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (image!.isEmpty) {
      return const Text('');
    }
    return Image.network(
      image!,
      height: 50.0,
      width: 50.0,
    );
  }
}
