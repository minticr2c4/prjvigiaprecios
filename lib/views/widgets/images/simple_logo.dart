import 'package:flutter/material.dart';

class SimpleLogo extends StatelessWidget {
  const SimpleLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Image(
      image: AssetImage('assets/images/vplogo.png'),
      height: 200.0,
      width: 200.0,
      // fit: BoxFit.fill,
    );
  }
}
