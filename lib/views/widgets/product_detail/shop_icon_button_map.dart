import 'package:flutter/material.dart';

class ShopIconButtonMap extends StatelessWidget {
  const ShopIconButtonMap({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {},
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
        side: const BorderSide(
          width: 1.0,
          color: Colors.blueGrey,
        ),
      ),
      child: const Icon(Icons.map_outlined),
    );
  }
}
