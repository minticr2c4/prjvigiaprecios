import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/models/entities/shop.dart';
import 'package:prjvigiaprecios/views/widgets/product_detail/shop_icon_button_map.dart';

class ShopDetail extends StatelessWidget {
  final Shop shop;
  const ShopDetail(this.shop, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(left: 1.0, top: 20.0),
              child: Row(
                children: <Widget>[
                  const Icon(Icons.add_location_alt_outlined),
                  const Text('  '),
                  Text(shop.address!,
                      style: const TextStyle(fontSize: 25, color: Colors.black))
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 1.0, top: 20.0),
              child: Row(
                children: <Widget>[
                  const Icon(Icons.alternate_email_rounded),
                  const Text('  '),
                  Text(shop.email ?? '',
                      style: const TextStyle(fontSize: 25, color: Colors.black))
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 1.0, top: 20.0),
              child: Row(
                children: <Widget>[
                  const Icon(Icons.contact_phone_outlined),
                  const Text('  '),
                  Text('${shop.phone}',
                      style: const TextStyle(fontSize: 25, color: Colors.black))
                ],
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Container(
              height: 50.0,
              width: 60.0,
              margin: const EdgeInsets.only(left: 1.0, top: 20.0, right: 10.0),
              child: const ShopIconButtonMap(),
            ),
            Container(
                margin: const EdgeInsets.only(left: 1.0, top: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const <Widget>[
                    Text('Lat  : ',
                        style: TextStyle(fontSize: 15, color: Colors.black)),
                    Text('Lon : ',
                        style: TextStyle(fontSize: 15, color: Colors.black)),
                  ],
                )),
            Container(
              margin: const EdgeInsets.only(top: 20.0, right: 10.0),
              child: Column(
                children: <Widget>[
                  Text('${shop.latitude}',
                      style:
                          const TextStyle(fontSize: 15, color: Colors.black)),
                  Text('  ${shop.longitude}',
                      style:
                          const TextStyle(fontSize: 15, color: Colors.black)),
                ],
              ),
            )
          ],
        )
      ],
    );
  }
}
