import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/views/widgets/images/brand_simple_logo.dart';

class ProductDetailTitle extends StatelessWidget {
  final String title;
  final String image;
  const ProductDetailTitle(this.title, this.image, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(right: 10.0),
          child: BrandSimpleLogo(image),
        ),
        Container(
            width: 200.0,
            margin: const EdgeInsets.only(left: 10.0),
            child: Expanded(
                child: Text(
              title,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              softWrap: false,
              style: const TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            )))
      ],
    );
  }
}
