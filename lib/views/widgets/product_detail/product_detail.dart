import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/models/entities/product_shop.dart';
import 'package:intl/intl.dart';

class ProductDetail extends StatelessWidget {
  final ProductShop productShop;
  const ProductDetail(this.productShop, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final numberFormat = NumberFormat.currency(locale: 'es_US', symbol: "\$");
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(top: 20.0),
          child: Row(
            children: <Widget>[
              const Icon(Icons.add_shopping_cart),
              const Text('  '),
              Text(productShop.product.name,
                  style: const TextStyle(
                      fontSize: 20,
                      color: Colors.black,
                      fontWeight: FontWeight.bold))
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 40.0, top: 20.0),
          child: Text(productShop.product.brand,
              style: const TextStyle(fontSize: 20, color: Colors.black)),
        ),
        Container(
          margin: const EdgeInsets.only(left: 40.0, top: 20.0),
          child: Text(productShop.product.description,
              style: const TextStyle(fontSize: 20, color: Colors.black)),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20.0),
          child: Row(
            children: <Widget>[
              const Icon(Icons.article_outlined),
              const Text('  '),
              Text(productShop.product.largeName,
                  style: const TextStyle(fontSize: 20, color: Colors.black))
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 30.0, right: 40.0),
          alignment: Alignment.centerRight,
          child: Text(numberFormat.format(productShop.amount),
              style: const TextStyle(
                  fontSize: 30,
                  color: Colors.green,
                  fontWeight: FontWeight.bold)),
        )
      ],
    );
  }
}
