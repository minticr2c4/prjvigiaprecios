import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/models/entities/search_product.dart';
import 'package:prjvigiaprecios/views/widgets/search_product_widgets/search_product_button_find.dart';
import 'package:prjvigiaprecios/models/entities/product.dart';
import 'package:prjvigiaprecios/controllers/request/products/product_controller.dart';

class SearchProductAutoComplete extends StatefulWidget {
  const SearchProductAutoComplete({Key? key}) : super(key: key);

  @override
  State<SearchProductAutoComplete> createState() =>
      _SearchProductAutoCompleteState();
}

class _SearchProductAutoCompleteState extends State<SearchProductAutoComplete> {
  static String _displayStringForOption(Product option) => option.name;

  late TextEditingController controller;
  late SearchProduct searchProduct = SearchProduct(name: '', category: '');

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Center(
            child: FutureBuilder<List<Product>?>(
                future: ProductController().getProducts(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState != ConnectionState.done) {
                    return const Center(child: CircularProgressIndicator());
                  }
                  if (snapshot.hasError) {
                    return Center(child: Text(snapshot.error.toString()));
                  }
                  if (!snapshot.hasData) {
                    return const Center(
                        child: Text("Products collection returns null!"));
                  }

                  List<Product> productsList = snapshot.data as List<Product>;

                  return Autocomplete<Product>(
                    displayStringForOption: _displayStringForOption,
                    optionsMaxHeight: 100.0,
                    optionsBuilder: (TextEditingValue textEditingValue) {
                      if (textEditingValue.text == '') {
                        return const Iterable<Product>.empty();
                      }
                      return productsList.where((Product option) {
                        return option
                            .searchBy()
                            .contains(textEditingValue.text.toLowerCase());
                      });
                    },
                    optionsViewBuilder:
                        (context, Function(Product) onSelected, options) {
                      return Material(
                        elevation: 4,
                        child: ListView.separated(
                          padding: EdgeInsets.zero,
                          itemBuilder: (context, index) {
                            final option = options.elementAt(index);

                            return ListTile(
                              title: Text(option.name),
                              subtitle: Text(option.category),
                              onTap: () {
                                onSelected(option);
                              },
                            );
                          },
                          separatorBuilder: (context, index) => const Divider(),
                          itemCount: options.length,
                        ),
                      );
                    },
                    onSelected: (Product selection) {
                      searchProduct.category = selection.category;
                      searchProduct.name = selection.name;
                    },
                    fieldViewBuilder:
                        (context, controller, focusNode, onEditingComplete) {
                      this.controller = controller;
                      return TextField(
                        controller: controller,
                        focusNode: focusNode,
                        onEditingComplete: onEditingComplete,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.grey[300]!),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.grey[300]!),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.grey[300]!),
                          ),
                          hintText: "Ej: arroz, grano",
                          prefixIcon: const Icon(Icons.search),
                        ),
                      );
                    },
                  );
                })),
        Container(
            margin: const EdgeInsets.only(top: 20.0, bottom: 50.0),
            child: SearchProductButtonSearch(searchProduct)),
      ],
    );
  }
}
