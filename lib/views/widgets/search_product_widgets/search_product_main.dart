import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/views/widgets/search_product_widgets/search_product_auto_complete.dart';

class SearchProductMain extends StatelessWidget {
  const SearchProductMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Container(
        margin: const EdgeInsets.only(top: 100.0, bottom: 20.0),
        child: const SearchProductTitle(),
      ),
      Container(
        margin: const EdgeInsets.only(
            left: 80.0, right: 80.0, top: 30.0, bottom: 70.0),
        child: const SearchProductAutoComplete(),
      ),
    ]);
  }
}

class SearchProductTitle extends StatelessWidget {
  const SearchProductTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
        child: Text(
      '¿Qué producto deseas consultar hoy?',
      style: TextStyle(
          fontSize: 30.0, fontWeight: FontWeight.bold, color: Colors.black),
    ));
  }
}
