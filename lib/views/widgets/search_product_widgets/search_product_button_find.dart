import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/views/pages/list_products_result/list_products_result_page.dart';
import 'package:prjvigiaprecios/models/entities/search_product.dart';
import 'package:prjvigiaprecios/views/prjbacklayout.dart';

class SearchProductButtonSearch extends StatelessWidget {
  const SearchProductButtonSearch(this.searchProduct, {Key? key})
      : super(key: key);

  final SearchProduct searchProduct;

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 25));

    return SizedBox(
      height: 50.0,
      width: 130.0,
      child: ElevatedButton(
        style: style,
        onPressed: () {
          if (searchProduct.name.isNotEmpty) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => PrjBackLayout(
                    content: ListProductsResultPage(searchProduct),
                    caption: 'Lista Productos')));
          }
        },
        child: const Text('Buscar'),
      ),
    );
  }
}
