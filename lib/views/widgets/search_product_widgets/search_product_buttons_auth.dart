import 'package:flutter/material.dart';

class SearchProductButtonLogin extends StatelessWidget {
  const SearchProductButtonLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));

    return SizedBox(
      height: 40.0,
      width: 180.0,
      child: ElevatedButton(
        style: style,
        onPressed: () {
          // Código para llevar a la página de inicio de sesión
        },
        child: const Text('Iniciar Sesión'),
      ),
    );
  }
}

class SearchProductButtonRegister extends StatelessWidget {
  const SearchProductButtonRegister({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));

    return SizedBox(
      height: 40.0,
      width: 180.0,
      child: ElevatedButton(
        style: style,
        onPressed: () {
          // Código para llevar a la página de registro
        },
        child: const Text('Crear Cuenta'),
      ),
    );
  }
}
