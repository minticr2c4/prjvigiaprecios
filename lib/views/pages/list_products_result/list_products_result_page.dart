import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/models/entities/search_product.dart';
import 'package:prjvigiaprecios/views/widgets/list_products_result/product_information_title.dart';
import 'package:prjvigiaprecios/views/widgets/list_products_result/product_information_drawing.dart';
import 'package:prjvigiaprecios/models/entities/product_shop.dart';
// import 'package:prjvigiaprecios/models/entities/shop.dart';
// import 'package:prjvigiaprecios/models/entities/product.dart';
import 'package:prjvigiaprecios/views/pages/product_detail/product_detail_page.dart';
import 'package:prjvigiaprecios/views/prjbacklayout.dart';
import 'package:prjvigiaprecios/controllers/request/prices/prices_controller.dart';
// import 'package:prjvigiaprecios/models/entities/price_entity.dart';

class ListProductsResultPage extends StatefulWidget {
  const ListProductsResultPage(this.searchProduct, {Key? key})
      : super(key: key);
  final SearchProduct searchProduct;

  @override
  State<ListProductsResultPage> createState() => _ListProductsResultPageState();
}

class _ListProductsResultPageState extends State<ListProductsResultPage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        FutureBuilder<List<ProductShop?>?>(
            future: PricesController().getPricesByName(widget.searchProduct),
            builder: (context, snapshot) {
              if (snapshot.connectionState != ConnectionState.done) {
                return const Center(child: CircularProgressIndicator());
              }
              if (snapshot.hasError) {
                return Center(child: Text(snapshot.error.toString()));
              }
              if (!snapshot.hasData) {
                return const Center(
                    child: Text("Prices collection returns null!"));
              }

              List<ProductShop> productsList =
                  snapshot.data as List<ProductShop>;

              return Column(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.center,
                        margin: const EdgeInsets.only(
                            left: 30.0, right: 30.0, top: 20.0),
                        child: ProductInformationTitle(productsList.length),
                      ),
                      for (ProductShop productShop in productsList)
                        ListProductButton(productShop)
                    ],
                  )
                ],
              );
            }),
      ],
    );
  }
}

class ListProductButton extends StatelessWidget {
  final ProductShop productShop;
  const ListProductButton(this.productShop, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
        child: OutlinedButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => PrjBackLayout(
                      content: ProductDetailPage(productShop),
                      caption: 'Detalle de Producto')));
            },
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              side: const BorderSide(
                width: 3.0,
                color: Colors.blueGrey,
              ),
            ),
            child: ProductInformationDrawing(productShop)));
  }
}
