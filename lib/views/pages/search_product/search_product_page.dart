import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/views/widgets/search_product_widgets/search_product_main.dart';
import 'package:prjvigiaprecios/views/widgets/images/simple_logo.dart';
import 'package:prjvigiaprecios/models/repositories/pruebas.dart';

class SearchProductPage extends StatelessWidget {
  const SearchProductPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Body();
  }
}

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(left: 80.0, right: 80.0, top: 50.0),
          child: const SimpleLogo(),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: const <Widget>[
            SearchProductMain(),
            Pruebas('VL2ZUMNy3XhlZhrZ4pUc'),
          ],
        )
      ],
    );
  }
}
