import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/controllers/request/register/register_controller.dart';

class RegistroRequest {
  late String fullname;
  late String email;
  late String? phone;
  late String password;

  @override
  String toString() {
    return "$fullname, $email, $phone, $password";
  }

  RegistroRequest(
      {required this.fullname,
      required this.email,
      this.phone,
      required this.password});
}

class Registro extends StatelessWidget {
  late final RegistroRequest _datos;
  late final RegisterController _register;

  Registro({super.key}) {
    _datos = RegistroRequest(fullname: '', email: '', phone: '', password: '');
    _register = RegisterController();
  }

  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();

    return Scaffold(
      body: Form(
        key: formKey,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                _basicWidget("Nombre", validarCampoObligatorio, (newValue) {
                  _datos.fullname = newValue!;
                }),
                _basicWidget("Correo Electronico", validarCampoObligatorio,
                    (newValue) {
                  _datos.email = newValue!;
                }),
                _basicWidget("Número Celular", (value) => null, (newValue) {
                  _datos.phone = newValue!;
                }),
                _basicWidget("Contraseña", validarCampoObligatorio, (newValue) {
                  _datos.password = newValue!;
                }),
                ElevatedButton(
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      String respuesta =
                          await _register.createUserWithEmailAndPassword(
                              _datos.email!, _datos.password!);

                      debugPrint('RESPUESTA REGISTRO $respuesta');
                      if (respuesta == "OK") {
                        // Si se crea correctamente
                      }

                      formKey.currentState!.save();
                    }
                  },
                  child: const Text("Listo!"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String? validarCampoObligatorio(String? value) {
    if (value == null || value.isEmpty) {
      return "El campo es obligatorio";
    }
    return null;
  }

  Widget _basicWidget(
    String title,
    FormFieldValidator<String?> validate,
    FormFieldSetter<String?> save,
  ) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        maxLength: 50,
        initialValue: "N/A",
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: title,
        ),
        validator: validate,
        onSaved: save,
      ),
    );
  }
}
