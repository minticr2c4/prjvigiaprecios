import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/controllers/common_validator.dart';
import '../../../controllers/request/shop_request.dart';
import '../../../controllers/shop_controller.dart';
import '../../widgets/common/datafield.dart';

class NewShop extends StatelessWidget {
  late final ShopRequest _data;
  late final ShopController _control;

  NewShop({super.key}) {
    _data = ShopRequest();
    _control = ShopController();
  }

  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                fld('Nombre', TextInputType.name, CommonValidator.required,
                    (val) {
                  _data.name = val!;
                }),
                fld('Latitud', TextInputType.number,
                    CommonValidator.mustBeCoords, (val) {
                  _data.latitude = double.parse(val!);
                }),
                fld('Longitud', TextInputType.number,
                    CommonValidator.mustBeCoords, (val) {
                  _data.longitude = double.parse(val!);
                }),
                fld('email', TextInputType.emailAddress,
                    CommonValidator.mustBeEmailNullable, (val) {
                  _data.email = val;
                }),
                fld('Dirección', TextInputType.streetAddress,
                    CommonValidator.nullable, (val) {
                  _data.address = val;
                }),
                fld('Teléfono', TextInputType.phone, CommonValidator.nullable,
                    (val) {
                  _data.phone = (val == '' ? null : val) as num?;
                }),
                fld('Imagen', TextInputType.text,
                    CommonValidator.mustBeUrlNullable, (val) {
                  _data.image = val;
                }),
                cmdSave(context, formKey),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget fld(
    String caption,
    TextInputType fldtype,
    FormFieldValidator<String?> validate,
    FormFieldSetter<String?> save,
  ) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: DataField(
        caption: caption,
        fldtype: fldtype,
        validate: validate,
        save: save,
      ),
    );
  }

  Widget cmdSave(BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          var msg = ScaffoldMessenger.of(context);
          try {
            final nav = Navigator.of(context);
            await _control.save(_data.toShop());
            msg.showSnackBar(
              const SnackBar(
                content: Text("La tienda ha sido registrada"),
              ),
            );
            nav.pop();
          } catch (e) {
            msg.showSnackBar(
              SnackBar(
                content: Text("Error: $e"),
              ),
            );
          }
          // print(_data.nombre);
        }
      },
      child: const Text(
        'Guardar',
        style: TextStyle(
          fontSize: 24,
        ),
      ),
    );
  }
}
