import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/controllers/common_validator.dart';

import '../../../controllers/request/product_request.dart';
import '../../../controllers/request/products/product_controller.dart';
import '../../widgets/common/datafield.dart';

class NewProduct extends StatelessWidget {
  late final ProductRequest _request;
  late final ProductController _controller;

  NewProduct({super.key}) {
    _request = ProductRequest();
    _controller = ProductController();
  }

  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                _fld('Nombre', TextInputType.name, CommonValidator.required,
                    (val) {
                  _request.name = val!;
                }),
                _fld('Marca', TextInputType.name, CommonValidator.required,
                    (val) {
                  _request.brand = val!;
                }),
                _fld('Categoría', TextInputType.name, CommonValidator.required,
                    (val) {
                  _request.category = val!;
                }),
                _fld('Nombre Largo', TextInputType.name,
                    CommonValidator.nullable, (val) {
                  _request.largeName = val!;
                }),
                _fld('Descripción', TextInputType.multiline,
                    CommonValidator.required, (val) {
                  _request.description = val!;
                }),
                cmdSave(context, formKey),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _fld(
    String caption,
    TextInputType type,
    FormFieldValidator<String?> validate,
    FormFieldSetter<String?> save,
  ) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: DataField(
        caption: caption,
        fldtype: type,
        validate: validate,
        save: save,
      ),
    );
  }

  Widget cmdSave(BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          var msg = ScaffoldMessenger.of(context);
          try {
            final nav = Navigator.of(context);
            await _controller.save(_request.toProduct());
            msg.showSnackBar(
              const SnackBar(
                content: Text("El producto ha sido registrado"),
              ),
            );
            nav.pop();
          } catch (e) {
            msg.showSnackBar(
              SnackBar(
                content: Text("Error: $e"),
              ),
            );
          }
          // print(_data.nombre);
        }
      },
      child: const Text(
        'Guardar',
        style: TextStyle(
          fontSize: 24,
        ),
      ),
    );
  }
}
