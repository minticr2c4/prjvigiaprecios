import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/models/entities/product_shop.dart';
import 'package:prjvigiaprecios/views/widgets/product_detail/product_detail_title.dart';
import 'package:prjvigiaprecios/views/widgets/product_detail/shop_detail.dart';
import 'package:prjvigiaprecios/views/widgets/product_detail/product_detail.dart';

class ProductDetailPage extends StatelessWidget {
  final ProductShop productShop;
  const ProductDetailPage(this.productShop, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(children: <Widget>[
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(left: 20.0, top: 20.0),
            child: ProductDetailTitle(
                productShop.shop.name, productShop.shop.image!),
          ),
          Container(
              margin: const EdgeInsets.only(left: 20.0, top: 10.0),
              child: ShopDetail(productShop.shop)),
          Container(
            margin: const EdgeInsets.only(left: 20.0, top: 20.0),
            child: ProductDetail(productShop),
          ),
        ],
      )
    ]);
  }
}
