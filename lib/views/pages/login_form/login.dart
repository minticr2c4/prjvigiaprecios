import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/controllers/login_controller.dart';
import 'package:prjvigiaprecios/controllers/request/login_request.dart';
import 'package:prjvigiaprecios/models/repositories/login_authentication.dart';
import 'package:prjvigiaprecios/views/widgets/common/datafield.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatelessWidget {
  late final LoginController _controller;
  late final LoginRequest _request;
  late final LoginAuthentication _auth;
  final _pref = SharedPreferences.getInstance();
  LoginPage({super.key}) {
    _controller = LoginController();
    _request = LoginRequest();
    _auth = LoginAuthentication();
  }

  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                _fld('Correo', TextInputType.emailAddress, _controller.nullable,
                    (val) {
                  _request.email = val!;
                }),
                _fld('Contraseña', TextInputType.visiblePassword,
                    _controller.nullable, (val) {
                  _request.password = val!;
                }, true),
                cmdValidate(context, formKey),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _fld(String caption, TextInputType fldtype,
      FormFieldValidator<String?> validate, FormFieldSetter<String?> save,
      [bool passMode = false]) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: DataField(
        caption: caption,
        fldtype: fldtype,
        validate: validate,
        save: save,
        passMode: passMode,
      ),
    );
  }

  Widget cmdValidate(BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          try {
            //print("validando");
            //msgout(context, "Validando");

            /*await _auth.signInEmailPassword(
                _request.email ?? "", _request.password ?? "");*/

            int codigoRespuesta = await _controller.validateUser(
                _request.email, _request.password);
            debugPrint('codigo respuesta login $codigoRespuesta');
            if (codigoRespuesta == 200) {}

            //pref.setBool("admin", info.isAdmin!);
          } catch (e) {
            msgout(context, e.toString());
          }
          // print(_data.nombre);
        }
      },
      child: const Text(
        'Ingresar',
        style: TextStyle(
          fontSize: 24,
        ),
      ),
    );
  }

  void msgout(BuildContext context, String msg) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(msg),
      ),
    );
  }
}
