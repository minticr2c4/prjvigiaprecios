// import 'dart:html';

import 'package:flutter/material.dart';
import 'package:prjvigiaprecios/views/pages/insert_forms/new_product.dart';
import 'package:prjvigiaprecios/views/pages/insert_forms/new_shop.dart';
import 'package:prjvigiaprecios/views/pages/login_form/login.dart';
import 'package:prjvigiaprecios/views/pages/registro_usuario/registro_usuario_page.dart';
import 'package:prjvigiaprecios/views/pages/search_product/search_product_page.dart';
import 'vp_favicon_icons.dart';
// import 'drawer.dart';

class PrjBackLayout extends StatelessWidget {
  final String caption;
  final Widget content;
  const PrjBackLayout(
      {super.key, required this.content, required this.caption});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text(caption),
          IconButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PrjBackLayout(
                      caption: 'Vigía de Precios',
                      content: SearchProductPage(),
                    ),
                  ));
            },
            icon: const Icon(VpFavicon.vpfavicon),
          ),
        ]),
      ),
      body: content,
      drawer: const BackMenu(),
    );
  }
}

class NoWidget extends StatelessWidget {
  const NoWidget({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: const [
      Text(''),
    ]));
  }
}

class BackMenu extends StatelessWidget {
  const BackMenu({super.key});
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            title: const Text('Consultar Precios'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => const PrjBackLayout(
                  caption: 'Vigía de Precios',
                  content: SearchProductPage(),
                ),
              ));
            },
          ),
          ListTile(
            title: const Text('Agregar Tienda'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) =>
                    PrjBackLayout(content: NewShop(), caption: 'Nueva Tienda'),
              ));
            },
          ),
          ListTile(
            title: const Text('Agregar Producto'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => PrjBackLayout(
                    content: NewProduct(), caption: 'Nuevo Producto'),
              ));
            },
          ),
          ListTile(
            title: const Text('Registrar Usuario'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      PrjBackLayout(content: Registro(), caption: 'Registro'),
                ),
              );
            },
          ),
          ListTile(
            title: const Text('Ingresar'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      PrjBackLayout(content: LoginPage(), caption: 'Ingreso'),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
